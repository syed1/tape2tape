import os

DEBUG = True

DB_BASEDIR = "tmp"

SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/coi.db'
SQLALCHEMY_MIGRATE_REPO = '/tmp/db_repository'

SQLALCHEMY_ECHO = True

SERVER_IP="0.0.0.0"
SERVER_PORT = 80

#Secret key for sessions
SECRET_KEY = 'my-secret-key'



