function goal_bars_teamA_home() {
	$.ajax({
		url: "http://localhost/api/getTotalGoals/OTTAWA/WASHINGTON",
		//getTotalGoals
		//getTotalWins
		//getTotalAttendance
		//getPowerPlay

		dataType: "json",
		success: function (data) {
			
				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 25;
				var side_text_padding = 20;
				var teamA = "OTTAWA";
				var teamB = "WASHINGTON";

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_home + data[teamB].goal_away])
								.range([0, width - 2*side_padding - split_padding]);


				var canvas = d3.select("body")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_home_goal = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_home));

				var tA1 = canvas.append("text")
							.text(data[teamA].goal_home)
							.attr("x", 0)
							.attr("y", vertical_text_padding);

				var teamB_away_goal  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width -  side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_away))
								.attr("x", width - widthScale(data[teamB].goal_away) - side_padding);

				var tB1 = canvas.append("text")
							.text(data[teamB].goal_away)
							.attr("x", width -  side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function goal_bars_teamB_home() {
	$.ajax({
		url: "http://localhost/api/getTotalGoals/OTTAWA/WASHINGTON",

		dataType: "json",
		success: function (data) {
			
				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 25;
				var side_text_padding = 20;
				var teamA = "OTTAWA";
				var teamB = "WASHINGTON";

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_away + data[teamB].goal_home])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("body")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_away_goal = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_away));

				var tA2 = canvas.append("text")
							.text(data[teamA].goal_away)
							.attr("x", 0)
							.attr("y", vertical_text_padding);				

				var teamB_home_goal  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_home))
								.attr("x", width - widthScale(data[teamB].goal_home) - side_padding);

				var tB2 = canvas.append("text")
							.text(data[teamB].goal_home)
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function goal_bars() {
	$.ajax({
		url: "http://localhost/api/getTotalGoals/OTTAWA/WASHINGTON",

		dataType: "json",
		success: function (data) {
			
				var teamA = "OTTAWA";
				var teamB = "WASHINGTON";

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 40;
				var side_text_padding = 10 + 10 * Math.min(20,Math.floor(log10(Math.max(data[teamA].goal_away + data[teamA].goal_home, 
														data[teamB].goal_away + data[teamB].goal_home))));

				console.log(side_text_padding);

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_away + data[teamA].goal_home + data[teamB].goal_away + data[teamB].goal_home])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("body")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_goals = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_away + data[teamA].goal_home));

				var tA2 = canvas.append("text")
							.text(data[teamA].goal_away + data[teamA].goal_home)
							.attr("x", 0)
							.attr("y", vertical_text_padding);				

				var teamB_goals  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_home + data[teamB].goal_away))
								.attr("x", width - widthScale(data[teamB].goal_home + data[teamB].goal_away) - side_padding);

				var tB2 = canvas.append("text")
							.text(data[teamB].goal_home + data[teamB].goal_away)
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function shot_bars() {
	$.ajax({
		url: "http://localhost/api/getTotalShots/OTTAWA/WASHINGTON",

		dataType: "json",
		success: function (data) {
			
				var teamA = "OTTAWA";
				var teamB = "WASHINGTON";

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 40;
				var side_text_padding = 10 + 10 * Math.min(20,Math.floor(log10(Math.max(data[teamA], 
														data[teamB]))));

				console.log(side_text_padding);

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_away + data[teamA].goal_home + data[teamB].goal_away + data[teamB].goal_home])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("body")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_goals = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_away + data[teamA].goal_home));

				var tA2 = canvas.append("text")
							.text(data[teamA].goal_away + data[teamA].goal_home)
							.attr("x", 0)
							.attr("y", vertical_text_padding);				

				var teamB_goals  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_home + data[teamB].goal_away))
								.attr("x", width - widthScale(data[teamB].goal_home + data[teamB].goal_away) - side_padding);

				var tB2 = canvas.append("text")
							.text(data[teamB].goal_home + data[teamB].goal_away)
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function log10(val) {
  return Math.log(val) / Math.LN10;
}