var availableTags = [
    "Anaheim Ducks",
        "Boston Bruins",
        "Buffalo Sabres",
        "Calgary Flames",
        "Carolina Hurricanes",
        "Chicago Blackhawks",
        "Colorado Avalanche",
        "Columbus Blue Jackets",
        "Dallas Stars",
        "Detroit Red Wings",
        "Edmonton Oilers",
        "Florida Panthers",
        "Los Angeles Kings",
        "Minnesota Wild",
        "Montreal Canadiens",
        "Nashville Predators",
        "New Jersey Devils",
        "New York Islanders",
        "New York Rangers",
        "Ottawa Senators",
        "Philadelphia Flyers",
        "Phoenix Coyotes",
        "Pittsburgh Penguins",
        "San Jose Sharks",
        "St. Louis Blues",
        "Tampa Bay Lightning",
        "Toronto Maple Leafs",
        "Vancouver Canucks",
        "Washington Capitals",
        "Winnipeg Jets"
    ];
    var availableTags1 = [
	"Anaheim Ducks",
        "Boston Bruins",
        "Buffalo Sabres",
        "Calgary Flames",
        "Carolina Hurricanes",
        "Chicago Blackhawks",
        "Colorado Avalanche",
        "Columbus Blue Jackets",
        "Dallas Stars",
        "Detroit Red Wings",
        "Edmonton Oilers",
        "Florida Panthers",
        "Los Angeles Kings",
        "Minnesota Wild",
        "Montreal Canadiens",
        "Nashville Predators",
        "New Jersey Devils",
        "New York Islanders",
        "New York Rangers",
        "Ottawa Senators",
        "Philadelphia Flyers",
        "Phoenix Coyotes",
        "Pittsburgh Penguins",
        "San Jose Sharks",
        "St. Louis Blues",
        "Tampa Bay Lightning",
        "Toronto Maple Leafs",
        "Vancouver Canucks",
        "Washington Capitals",
        "Winnipeg Jets"
    ];
    var availableTags2 = [
        "Anaheim Ducks",
        "Boston Bruins",
        "Buffalo Sabres",
        "Calgary Flames",
        "Carolina Hurricanes",
        "Chicago Blackhawks",
        "Colorado Avalanche",
        "Columbus Blue Jackets",
        "Dallas Stars",
        "Detroit Red Wings",
        "Edmonton Oilers",
        "Florida Panthers",
        "Los Angeles Kings",
        "Minnesota Wild",
        "Montreal Canadiens",
        "Nashville Predators",
        "New Jersey Devils",
        "New York Islanders",
        "New York Rangers",
        "Ottawa Senators",
        "Philadelphia Flyers",
        "Phoenix Coyotes",
        "Pittsburgh Penguins",
        "San Jose Sharks",
        "St. Louis Blues",
        "Tampa Bay Lightning",
        "Toronto Maple Leafs",
        "Vancouver Canucks",
        "Washington Capitals",
        "Winnipeg Jets"
    ];
    var team_codes =[
        "ANAHEIM",
        "BOSTON",
        "BUFFALO",
        "CALGARY",
        "CAROLINA",
        "CHICAGO",
        "COLORADO",
        "COLUMBUS",
        "DALLAS",
        "DETROIT",
        "EDMONTON",
        "FLORIDA",
        "LOS ANGELES",
        "MINNESOTA",
        "MONTREAL",
        "NASHVILLE",
        "NEW JERSEY",
        "NY ISLANDERS",
        "NY RANGERS",
        "OTTAWA",
        "PHILADELPHIA",
        "PHOENIX",
        "PITTSBURGH",
        "SAN JOSE",
        "ST LOUIS",
        "TAMPA BAY",
        "TORONTO",
        "VANCOUVER",
        "WASHINGTON",
        "WINNIPEG"
    ];

$(function() {
 
     $('#homeTeam').focusin(function(){
        var i = availableTags.indexOf($('#awayTeam').val());
        if(i != -1) {
           availableTags1 = availableTags.slice(0);
	       availableTags1.splice(i, 1);
           availableTags2 = availableTags.slice(0);
        }
        $( "#homeTeam" ).autocomplete({
            source: availableTags1
        });     
    });
    
    
    $('#awayTeam').focusin(function(){
        var i = availableTags.indexOf($('#homeTeam').val());
        if(i != -1) {
           availableTags2 = availableTags.slice(0);
	       availableTags2.splice(i, 1);
           availableTags1 = availableTags.slice(0);
        }
        $( "#awayTeam" ).autocomplete({
            source: availableTags2
        });     
    });

});

