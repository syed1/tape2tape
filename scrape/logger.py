import logging

logger = logging.getLogger('coi')

def init_logging() :

    logger.setLevel(logging.DEBUG)

    #log to file
    #fh = logging.FileHandler(settings.LOG_FILE)
    #fh.setLevel(logging.DEBUG)

    #log to console
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    #log to mongodb
    #dbh = MongoHandler(host='localhost')

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    #logger.addHandler(fh)
    logger.addHandler(ch)
    #logger.addHandler(dbh)

init_logging()

