from logger import logger as log
from bs4 import BeautifulSoup as bs
from pymongo import MongoClient
import requests
import pprint
import time


BASE_URL = "http://www.nhl.com"

connection = MongoClient("tapetotape.info")
db = connection.tapetotape

def _parseInt(d):
    d = d.strip().replace(',','')
    return int(d)

def retrievePage(url):
    """ Get the source for the page
    Should handle any JS if required
    """

    data = requests.get(url)
    #import ipdb; ipdb.set_trace()
    return data.text


def parseMatchData(data):
    """
    Parses the main match data

    returns a dict which has all the data
    see README for the format

    """
    pass



def parseMatchLinks(data):
    """
    Returns a list of html pages which contain 
    match data
    """
    soup = bs(data)
    table = soup.find('table', class_="data stats")
    rows = table.find('tbody').find_all('tr')

    # matches table index to data attribute
    td_mapping = { 0: "stats_url",
            1: "visitor_name", 
            2: "visitor_goals",
            3: "home_name", 
            4: "home_goals",
            5: "win_type",
            6: "win_goalie",
            7: "win_goal",
            8: "visitor_shots",
            9: "visitor_ppgf", #XXX: ???
            10: "visitor_ppopp", # XXX ???
            11: "visitor_pim",
            12: "home_shots",
            13: "home_ppgf",
            14: "home_ppopp",
            15: "home_pim", 
            16: "attendance"
        }
    
    stats = list()
    for row in rows:
        data = dict()
        tds = row.find_all('td')
        for i in range(0,17):
            if td_mapping[i] == "stats_url":
                link = tds[i].find('a').get('href')
                data[td_mapping[i]] = link
                data['date'] = tds[i].find('a').text


            if td_mapping[i] not in ["visitor_name", "home_name", "win_goalie",
                    "win_goal", "win_type", "stats_url"]:
                data[td_mapping[i]] = _parseInt(tds[i].text)
            else:
                data[td_mapping[i]] = tds[i].text.strip()

        stats.append(data) 

    return stats

def convertToDbFormat(data):
    """ Converts from data we get from the page to 
    mongodb schema data
    """
    mongo_data = { "home" : dict(), "visitor": dict()}

    for key in mongo_data:
        key_dict = dict()
        for val in ["name", "goals", "shots", "ppgf", "ppopp", "pim"]:
            key_dict[val] = data[key + "_" + val]
        mongo_data[key] = key_dict

    mongo_data["attendance"] =  data["attendance"] 
    mongo_data["_id"] = data["date"].replace("'", '').replace(' ', '_') + '_' + data['home_name'] + '_' + data['visitor_name']
    mongo_data["win_goalie"] =  data["win_goalie"] 
    mongo_data["win_goal"] =  data["win_goal"] 
    mongo_data["win_type"] = data["win_type"]
    mongo_data["date"] = data["date"]
    if not mongo_data["win_type"]:
        mongo_data["win_type"] = "NML"
    

    pprint.pprint(mongo_data)
    return mongo_data

def getNextPage(source):
    soup = bs(source)
    next_link = soup.find('a', text="Next")
    if not next_link:
        return None

    return BASE_URL + next_link.get('href')

def storeMatchToDb(db_data):
    log.debug(db_data)
    db.matches.save(db_data)

def startParser():

    START_URL = "http://www.nhl.com/ice/gamestats.htm?season=%s&gameType=2&team=&viewName=summary#"
    season = 2005
    while season < 2014:
        season_str = "%s%s"%(season, season+1)
        next_url =  START_URL % ( season_str )
        log.debug("Starting parser " + next_url)
        while next_url:
            source = retrievePage(next_url)
            stats = parseMatchLinks(source)
            log.debug(pprint.pprint(stats))
            for stat in stats:
                db_data = convertToDbFormat(stat)
                storeMatchToDb(db_data)
            next_url = getNextPage(source)
            time.sleep(0.5)
        
        season += 1
        time.sleep(5)


if __name__ == "__main__":

    startParser()

