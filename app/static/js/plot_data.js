function plotData(homeTeam, awayTeam) {
	$.ajax({
		url: "/api/getShotsPerMatch/" + homeTeam + "/" + awayTeam,
		dataType: "json",
		success: function (data) {
		
			var width = 960;
			var height = 500;
			var axisPos = 250;
			var padding = 30;

			var teamA = homeTeam;
			var teamB = awayTeam;
			var shots_data = data.games;
			var maxShotDiff = 0;

			for(var i=0; i<shots_data.length; i++)
			{

				if(Math.abs(shots_data[i][teamA] - shots_data[i][teamB]) > maxShotDiff)
				{
					maxShotDiff = Math.abs(shots_data[i][teamA] - shots_data[i][teamB]);
				}
			}

			maxShotDiff += 2;

			var heightScale = d3.scale.linear()
								.domain([maxShotDiff, -maxShotDiff])
								.range([0, height-40]);
			var widthScale = d3.scale.linear()
								.domain([0,shots_data.length])
								.range([0, width]);
			var axisScale = d3.scale.linear()
								.domain([maxShotDiff, -maxShotDiff])
								.range([0, height-40]);


			var yAxis = d3.svg.axis()
						.orient("left")
						.scale(axisScale);



			var line = d3.svg.line()
			.interpolate("cardinal")
			.x(function(d,i) {  return widthScale(i) })
			.y(function(d) { return heightScale(d[teamA] - d[teamB]) });

			var canvas = d3.select("#shots_per_match")
			.append("svg")
			.attr("width", width)
			.attr("height", height)
			.append("g")
			//.attr("transform", "translate(20, 20)");

			var group = canvas.append("g")
			 .attr("transform", "translate(50, 20)")
			 .call(yAxis);
	

			//group.selectAll("path")
			var path = canvas.append("path")
						.attr("transform", "translate(50, 20)")
						.attr("d", line(shots_data))
      .attr("stroke", "steelblue")
      .attr("stroke-width", "2")
      .attr("fill", "none");

			//.data([data.attendance])
			//.enter()
		
			//.append("path")
			/*.attr("d", line)
			.attr("fill", "none")
			.attr("stroke", "green")
			.attr("stroke-width", 10);*/
			

			/*var totalLength = path.node().getTotalLength();
  path
      .attr("stroke-dasharray", totalLength + " " + totalLength)
      .attr("stroke-dashoffset", totalLength)
      .transition()
        .duration(2000)
        .ease("linear")
        .attr("stroke-dashoffset", 0);
*/
			canvas.append("g")
			.attr("transform", "translate(0, " + (axisPos + 20) + ")")
			.append("rect")
			.attr("width", width)
			.attr("height", 2);
			//.call(axis);

		//var cover = canvas.append("rect").attr("width", 960).attr("height", 400).attr("x", 0).attr("fill", "white")
		//		.transition().duration(3000).attr("x", 960);


		}
	});

}
