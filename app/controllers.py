import sys
from flask import make_response, render_template, g, request, session 
from app import app  
import json
from datetime import datetime


from flask import request, current_app


from pymongo import MongoClient
connection = MongoClient("tapetotape.info")
db = connection.tapetotape

def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()



@app.route('/jamie')
def jamie_test():
    return render_template("jamie_test.html")

@app.route("/")
def index():
    return render_template("index.html")

def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()


###### API #####

@app.route("/api/getTotalGoals/<team1>/<team2>")
def getTotalGoals(team1, team2):

    ret = { team1: {"goal_home":0 , "goal_away":0}, 
            team2: {"goal_home":0 , "goal_away":0}
        }

    matches = db.matches.find({ "home.name": team1, "visitor.name": team2})

    for match in matches:
        ret[team1]["goal_home"] += match['home']['goals']
        ret[team2]["goal_away"] += match['visitor']['goals']

    matches = db.matches.find({ "home.name": team2, "visitor.name": team1})

    for match in matches:
        ret[team2]["goal_home"] += match['home']['goals']
        ret[team1]["goal_away"] += match['visitor']['goals']



    return json.dumps(ret)

@app.route("/api/getTotalAttendance/<team1>/<team2>")
def getTotalAttendance(team1, team2):
    

    matches = q = db.matches.find( { "$or":[{"home.name": team1, "visitor.name": team2},
                                      {"visitor.name": team1, "home.name": team2}]
                                    })
    ret = { "attendance" : list() }
    for match in matches:
        ret["attendance"].append({ "date" : match['date'], 'attendance': match['attendance']})

    return json.dumps(ret)

@app.route("/api/getTotalWins/<team1>/<team2>")
def getTotalWins(team1, team2):

    matches = q = db.matches.find( { "$or":[{"home.name": team1, "visitor.name": team2},
                                      {"visitor.name": team1, "home.name": team2}]
                                    })

   
    ret = { team1: 0, team2: 0}


    for match in matches:
        home_goals = match["home"]["goals"]
        visitor_goals  = match["visitor"]["goals"]
        home_team = match["home"]["name"]
        visitor_team = match["visitor"]["name"]
        win_type = match["win_type"]


        if home_goals > visitor_goals: 
            # home win
            ret[home_team] += 1
        elif visitor_goals > home_goals:
            # visitor win
            ret[visitor_team] += 1

    return json.dumps(ret)

@app.route('/api/getPowerPlay/<team1>/<team2>')
def getPowerPlayPercent(team1,team2):
    matches = db.matches.find( { "$or":[{"home.name": team1, "visitor.name": team2},
                                      {"visitor.name": team1, "home.name": team2}]
                                    })
    ret = { team1: { "ppgf" : 0 , "ppopp": 0 },
            team2: { "ppgf" : 0 , "ppopp": 0 }
          }

    for match in matches:
        for team in [match["home"], match["visitor"]]:
            team_name = team["name"]
            ret[team_name]["ppgf"] += team["ppgf"]
            ret[team_name]["ppopp"] += team["ppopp"]

 
    return json.dumps(ret)


@app.route('/api/getAvgAttendance/<team1>/<team2>')
def getAvgAttendance(team1,team2):
    matches = db.matches.find({"home.name": team1, "visitor.name": team2})
    ret = {team1: 0, team2: 0}
    avg_attend = 0
    for match in matches:
        avg_attend += match['attendance']
    ret[team1] = float(avg_attend)/float(matches.count())
    

    matches = db.matches.find({"home.name": team2, "visitor.name": team1})
    avg_attend = 0
    for match in matches:
        avg_attend += match['attendance']
    ret[team2] = float(avg_attend)/float(matches.count())


    return json.dumps(ret)
  
@app.route('/api/getTotalShots/<team1>/<team2>')
def getTotalShots(team1, team2):
    matches = db.matches.find( { "$or":[{"home.name": team1, "visitor.name": team2},
                                      {"visitor.name": team1, "home.name": team2}]
                                    })
    ret = { team1: 0, team2: 0}
    for match in matches:
        for team in ["home", "visitor"]:
            team_name = match[team]['name']
            shots = match[team]['shots']
            ret[team_name] += shots

    return json.dumps(ret)


@app.route('/api/getShotsPerMatch/<team1>/<team2>')
def getShotsPerMatch(team1,team2):

    def dateSorter(m):
        dt = datetime.strptime(m['date'] , "%b %d '%y")
        return unix_time(dt)

    matches = db.matches.find( { "$or":[{"home.name": team1, "visitor.name": team2},
                                      {"visitor.name": team1, "home.name": team2}]
                                    })

    ret = {}
    games = list()
    for match in matches:
        games.append({ "date" : match["date"], 
                        match["home"]["name"]: match["home"]["shots"], 
                        match["visitor"]["name"]: match["visitor"]["shots"]
                    })

    games = sorted(games, key=dateSorter, reverse=False)

    return json.dumps({ "games": games })
