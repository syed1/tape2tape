function getTeamLogos(homeTeam, awayTeam) {
    var homeURL = '/static/logos/' + homeTeam + '.png';

    console.log(homeURL);

    var img1 = $("<img />").attr('src', homeURL )
    .load(function() {
            console.log("Loaded home image");
            $('#homeLogo').html('');
            $("#homeLogo").append(img1);
    });

    var img2 = $("<img />").attr('src', '/static/logos/' + awayTeam + '.png')
    .load(function() {
            $('#awayLogo').html('');
            $("#awayLogo").append(img2);
    });

}

function getGoals(homeTeam, awayTeam){

    $.ajax({
        url: "/api/getTotalGoals/" + homeTeam + "/" + awayTeam,
        dataType: "json",
        success: function(data){
            
            $('#homeGoals').html('<h4>' + (data[homeTeam].goal_home + data[homeTeam].goal_away) + ' Goals </h4>');
            $('#awayGoals').html('<h4>' + (data[awayTeam].goal_home + data[awayTeam].goal_away) + ' Goals </h4>');
        }

    });

}

function getGamesWon(homeTeam, awayTeam){

    $.ajax({
        url: "/api/getTotalWins/" + homeTeam + "/" + awayTeam,
        dataType: "json",
        success: function(data){
            $('#homeWon').html('<h1>' + data[homeTeam] + '</h1> <span class="win">Wins</span>');
            $('#awayWon').html('<h1>' + data[awayTeam] + '</h1> <span class="win">Wins</span>');
            console.log(data);
        }

    });

}

function fillStats1(homeTeam, awayTeam){
    getTeamLogos(homeTeam, awayTeam);
    getGoals(homeTeam, awayTeam);
    getGamesWon(homeTeam, awayTeam);
}

function fillStats2(homeTeam, awayTeam){
    $("#ppg_bar_div").html('')
    ppg_bar(homeTeam, awayTeam);

    $('#shot_bar_div').html('');
    shot_bar(homeTeam, awayTeam);

    $('#total_goals_bar_div').html('');
    goal_bar(homeTeam, awayTeam);
}

function fillStats3(homeTeam, awayTeam){

    $("#homeSpeedometer").html("");
    speedometer(homeTeam, awayTeam, "#homeSpeedometer");

    $("#awaySpeedometer").html("");
    speedometer(awayTeam, homeTeam, "#awaySpeedometer");
        
}

function fillStats4(homeTeam,awayTeam){

    $('#shots_per_match').html("");
    plotData(homeTeam, awayTeam);
}

$(document).ready(function(){

    $('#submitTeams').click( function(e){
        console.log('Submit teams');
        e.preventDefault();
        effect_options = { "margin-top": "-=500px"} 

        $('#searchTitle').animate(effect_options, "slow");
        $('#accordion1').fadeIn("slow");
        $('#accordion2').fadeIn("slow");
        $('#accordion3').fadeIn("slow");
        $('#accordion4').fadeIn("slow");

        //homeTeam = $('#homeTeam').val();
        //awayTeam = $('#awayTeam').val();

        var i = availableTags.indexOf($('#homeTeam').val());
        var j = availableTags.indexOf($('#awayTeam').val());
        var homeTeam = team_codes[i];
        var awayTeam = team_codes[j];
        // TODO: Call all the API functions here
        fillStats1(homeTeam, awayTeam);
        fillStats2(homeTeam, awayTeam);
        fillStats3(homeTeam, awayTeam);
        fillStats4(homeTeam, awayTeam);

    });

    $('#accordion1').hide()
    $('#accordion2').hide()
    $('#accordion3').hide()
    $('#accordion4').hide()


});
