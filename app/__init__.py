from flask import Flask
#from flask.ext.openid import OpenID

app = Flask(__name__)

from app import controllers

app.config.from_object("app.settings")
app.url_map.strict_slashes = False


