function speedometer(homeTeam, awayTeam, attachDivId) {
$.ajax({
        url: "/api/getAvgAttendance/" + homeTeam + "/" + awayTeam,
        dataType: "json",
        success: function (data) {

                var capacities = {
                        "ANAHEIM": 17610,
                        "BOSTON": 17565,
                        "BUFFALO": 19070,
                        "CALGARY": 19289,
                        "CAROLINA": 19016,
                        "CHICAGO": 22428,
                        "COLORADO": 18646,
                        "COLUMBUS": 19219,
                        "DALLAS": 19099,
                        "DETROIT": 20066,
                        "EDMONTON": 16839,
                        "FLORIDA": 20741,
                        "LOS ANGELES": 18867,
                        "MINNESOTA": 19893,
                        "MONTREAL": 21273,
                        "NASHVILLE": 17322,
                        "NEW JERSEY": 17625,
                        "NY ISLANDERS": 16170,
                        "NY RANGERS": 18006,
                        "OTTAWA": 20510,
                        "PHILADELPHIA": 20327,
                        "PHOENIX": 17746,
                        "PITTSBURGH": 18673,
                        "SAN JOSE": 17562,
                        "ST LOUIS": 20082,
                        "TAMPA BAY": 19204,
                        "TORONTO": 19746,
                        "VANCOUVER": 18910,
                        "WASHINGTON": 18506,
                        "WINNIPEG": 15004
                }

                var svg = d3.select(attachDivId)
                                .append("svg:svg")
                                .attr("width", 400)
                                .attr("height", 400);


                        var gauge = iopctrl.arcslider()
                                .radius(120)
                                .events(false)
                                .indicator(iopctrl.defaultGaugeIndicator);
                        gauge.axis().orient("in")
                                .normalize(true)
                                .ticks(12)
                                .tickSubdivide(3)
                                .tickSize(10, 8, 10)
                                .tickPadding(5)
                                .scale(d3.scale.linear()
                                        .domain([0, 100])
                                        .range([-3*Math.PI/4, 3*Math.PI/4]));

                        var segDisplay = iopctrl.segdisplay()
                                .width(80)
                                .digitCount(6)
                                .negative(false)
                                .decimals(0);

                        svg.append("g")
                                .attr("class", "segdisplay")
                                .attr("transform", "translate(130, 200)")
                                .call(segDisplay);

                        svg.append("g")
                                .attr("class", "gauge")
                                .call(gauge);

                        segDisplay.value(data[homeTeam]);
                        gauge.value(100*data[homeTeam]/capacities[homeTeam]);
                }
        });
}