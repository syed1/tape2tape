#!/usr/bin/env python
import sys
from app import app
app.run(app.config.get('SERVER_IP'), 
        app.config.get('SERVER_PORT'),
        debug=True)
