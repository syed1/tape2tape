function goal_bar_teamA_home() {
	$.ajax({
		url: "http://localhost/api/getTotalGoals/OTTAWA/WASHINGTON",
		//getTotalGoals
		//getTotalWins
		//getTotalAttendance
		//getPowerPlay

		dataType: "json",
		success: function (data) {
			
				var teamA = "OTTAWA";
				var teamB = "WASHINGTON";

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 45;
				var side_text_padding = 40;
				var left_text_padding = (30 - 10 * Math.max(1,Math.floor(log10(Math.max(data[teamB].goal_away, 
														data[teamA].goal_home)))));

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_home + data[teamB].goal_away])
								.range([0, width - 2*side_padding - split_padding]);


				var canvas = d3.select("body")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_home_goal = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_home));

				var text1 = canvas.append("text")
							.text(data[teamA].goal_home)
							.attr("x", left_text_padding)
							.attr("y", vertical_text_padding);

				var teamB_away_goal  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width -  side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_away))
								.attr("x", width - widthScale(data[teamB].goal_away) - side_padding);

				var text2 = canvas.append("text")
							.text(data[teamB].goal_away)
							.attr("x", width -  side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function goal_bar_teamB_home() {
	$.ajax({
		url: "http://localhost/api/getTotalGoals/OTTAWA/WASHINGTON",

		dataType: "json",
		success: function (data) {
			
				var teamA = "OTTAWA";
				var teamB = "WASHINGTON";

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 45;
				var side_text_padding = 40;
				var left_text_padding = (30 - 10 * Math.max(1,Math.floor(log10(Math.max(data[teamA].goal_away, 
														data[teamB].goal_home)))));

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_away + data[teamB].goal_home])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("body")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_away_goal = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_away));

				var text1 = canvas.append("text")
							.text(data[teamA].goal_away)
							.attr("x", left_text_padding)
							.attr("y", vertical_text_padding);				

				var teamB_home_goal  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_home))
								.attr("x", width - widthScale(data[teamB].goal_home) - side_padding);

				var text2 = canvas.append("text")
							.text(data[teamB].goal_home)
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function goal_bar(homeTeam, awayTeam) {
	$.ajax({
		url: "/api/getTotalGoals/" + homeTeam + "/" + awayTeam,

		dataType: "json",
		success: function (data) {
			
				var teamA = homeTeam;
				var teamB = awayTeam;

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 45;
				var side_text_padding = 40
				var left_text_padding = (30 - 10 * Math.max(1,Math.floor(log10(Math.max(data[teamA].goal_away + data[teamA].goal_home, 
														data[teamB].goal_away + data[teamB].goal_home)))));

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].goal_away + data[teamA].goal_home + data[teamB].goal_away + data[teamB].goal_home])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("#total_goals_bar_div")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_goals = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].goal_away + data[teamA].goal_home));

				var text1 = canvas.append("text")
							.text(data[teamA].goal_away + data[teamA].goal_home)
							.attr("x", left_text_padding)
							.attr("y", vertical_text_padding);				

				var teamB_goals  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].goal_home + data[teamB].goal_away))
								.attr("x", width - widthScale(data[teamB].goal_home + data[teamB].goal_away) - side_padding);

				var text2 = canvas.append("text")
							.text(data[teamB].goal_home + data[teamB].goal_away)
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function shot_bar(homeTeam, awayTeam) {
	$.ajax({
		url: "/api/getTotalShots/" + homeTeam + "/" + awayTeam,

		dataType: "json",
		success: function (data) {
			
				var teamA = homeTeam;
				var teamB = awayTeam;

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 45;
				var side_text_padding = 40;
				var left_text_padding = (30- 10 * Math.max(1,Math.floor(log10(Math.max(data[teamA], data[teamB])))));

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA] + data[teamB]])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("#shot_bar_div")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_goals = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA]));

				var text1 = canvas.append("text")
							.text(data[teamA])
							.attr("x", left_text_padding)
							.attr("y", vertical_text_padding);				

				var teamB_goals  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB]))
								.attr("x", width - widthScale(data[teamB]) - side_padding);

				var text2 = canvas.append("text")
							.text(data[teamB])
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function ppg_bar(homeTeam, awayTeam) {
	$.ajax({
		url: "/api/getPowerPlay/" + homeTeam + "/" + awayTeam,

		dataType: "json",
		success: function (data) {
			
				var teamA =  homeTeam;
				var teamB =  awayTeam;

				var height = 50;
				var width = 500;
				var bar_height = 25;
				var split_padding = 8;
				var vertical_padding = 10;
				var vertical_text_padding = 20
				var side_padding = 45;
				var side_text_padding = 40;
				var left_text_padding = (30- 10 * Math.max(1,Math.floor(log10(Math.max(data[teamA].ppgf, data[teamB].ppgf)))));

				var widthScale = d3.scale.linear()
								.domain([0,data[teamA].ppgf + data[teamB].ppgf])
								.range([0, width - 2*side_padding - split_padding]);

				var canvas = d3.select("#ppg_bar_div")
				.append("svg")
				.attr("width", width)
				.attr("height", height);

				var teamA_goals = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("y", 0)
								.attr("x",  side_padding)
								.attr("fill", "#B2182B")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamA].ppgf));

				var text1 = canvas.append("text")
							.text(data[teamA].ppgf)
							.attr("x", left_text_padding)
							.attr("y", vertical_text_padding);				

				var teamB_goals  = canvas.append("rect")
								.attr("width", 0)
								.attr("height", bar_height)
								.attr("x", width - side_padding)
								.attr("y", 0)
								.attr("fill", "#898989")
								.transition()
								.duration(2000)
								.attr("width", widthScale(data[teamB].ppgf))
								.attr("x", width - widthScale(data[teamB].ppgf) - side_padding);

				var text2 = canvas.append("text")
							.text(data[teamB].ppgf)
							.attr("x", width - side_text_padding)
							.attr("y", vertical_text_padding);


		}
   });
}

function log10(val) {
  return Math.log(val) / Math.LN10;
}